\documentclass{article}
\usepackage[margin=3cm]{geometry}
\usepackage[autostyle, english = american]{csquotes}
\usepackage[labelfont=bf]{caption}
\usepackage{framed}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{geometry}
\usepackage{etoolbox}
\patchcmd{\thebibliography}{\section*{\refname}}{}{}{}
\usepackage{caption}
\usepackage{sidecap}
\usepackage{url}
\usepackage{natbib}
\usepackage{mathptmx}
\usepackage{tabu}
\usepackage{times}
\usepackage{amsmath}
\usepackage{color}
\usepackage{algpseudocode}
\usepackage{amsthm}
\usepackage{enumerate}
\usepackage{mathtools}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\pagestyle{empty}
\newcommand{\FormItem}[2]{\noindent {\bfseries #1}: #2 \\}


\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}

\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

\author{Alastair Gregory (Imperial) and Onno Bokhove (Leeds).}

\begin{document}
\title{\Large {\bfseries Understanding the effects of spatial rainfall patterns on flood events}\\{\textit{Maths Foresees \& Turing Gateway Study Group}\\} \hspace{3mm}
Environment Agency Challenge Report}

\maketitle




\begin{figure}[h!]
\centering
\includegraphics[width=8cm]{ealogo.jpg}
\end{figure}


\vspace{-10mm}

\begin{figure}[h!]
\centering
\includegraphics[width=3cm]{mflogo}
\end{figure}


\begin{figure}[h!]
\centering
\includegraphics[width=8cm]{tgmlogo.jpg}
\end{figure}

\bigskip
\bigskip
\bigskip

\centerline{Challenge presented by: Adam Baylis}\\
\centerline{Contributors: J. Vanneste, A. Osojnik, O. Sheridan-Methven, A. Chen, S. Patidar, P. Sertyesilisik and C. Cafaro}

\newpage

\tableofcontents

\newpage

\section{Introduction and motivation}

Understanding rainfall patterns is crucial in forecasting extreme flood events. Just as the temporal variations in these patterns is important in being able to forecast clustered flood events over only a short period (resulting in major flood damage), the spatial variation over the entire catchment is also. Many flood models choose to iradicate this variation, and model rainfall as being uniformly distributed over the catchment. This includes the Environment Agency's initial challenge specification \citep{EA}. However this is not always the case in reality, and the consequences of failing to model a strongly skewed distribution in one spatial direction could be grave.

\subsection{Spatially varying rainfall patterns}

\label{sec:testcases}

Ideally, we would like to be able to simulate rainfall that is spatio-temporally distributed in these three test cases:

\begin{enumerate}

\item[(1)] Spatially uniform rainfall.

\item[(2)] Spatially varying rainfall (e.g. Canvey Island \citep{EA}).

\item[(3)] Temporally and spatially varying rainfall (e.g. Medway \citep{Lamb}).

\end{enumerate}



\subsection{Probabilistic forecasting of flood events}

Probabilistic forecasting is commonplace for flood prediction \citep{Demeritt, Cloke}. In this setting it is traditionally done with ensemble-based methods. Ensemble-based methods of probabilistic flood forecasting can incorporate spatio-temporal variation in rainfall patterns naturally. Once we can simulate from spatially and temporally varying rainfall patterns, this incorporation can be visualised via the following process flow:


$$
\textbf{{\color{red}Input: Sampling rainfall events}} \quad \longrightarrow \quad \underbrace{\textbf{Black-box flood model}}_{expensive!} \quad \longrightarrow\quad  \textbf{{\color{blue}Output: Postprocess / forecast}}
$$

We will break this flow down into three part descriptions.

\bigskip

\FormItem{Sampling rainfall events}{
The first part of this process flow describes the sampling of rainfall events from a spatio-temporal distribution. The distribution that one samples from here is primarily dependent on the assumptions that one makes about a certain catchment's rainfall patterns. Typical behaviour of a catchment's rainfall patterns should be known and a distribution could be inferred from this. Clearly, the more complicated the distribution (higher numbers of degrees of freedom), the more challenging the forecasting problem becomes. This is because a fixed sized ensemble of simulations will be less representative of the true likelihood of rare flood events in this case. This stage of the process is the one that we propose to optimise in this work.
}

\bigskip

\FormItem{Black-box flood model}{
The next part of the forecasting process contains most of the leg-work. One must usually run some black-box flood model with inputs (source term, topography) over a time interval and in space over the catchment / domain. This is by far the most computationally expensive part of the forecasting process. In some cases this black-box model will be the shallow water equations (SWEs) although this is a particularly expensive choice \citep{Ern, Gregory, Cea}. Later in this report we concentrate on a simpler one-dimensional flood model (Wetropolis) in this part of the forecasting process.

}

\bigskip

\FormItem{Postprocess and forecast}{
This final part usually takes the form of a statistical postprocessing / forecasting method. For example, given an ensemble of simulations derived from the last part (black-box model), one can compute statistics of flood forecast via Monte Carlo methods. Here, a variety of verification / scoring techniques can be used to evaluate the quality / performance of the forecast \citep{Gneiting07}.

}

\bigskip

There are plenty of ways to reduce the computational complexity of the above process to arrive at feasible forecasts. Variance reduction is a popular choice when using Monte Carlo in the estimating statistics to forecasts; multilevel methods have raised significant interest in the last decade. In fact, a Maths Foresees Network feasibility study looked into applying this particular variance reduction technique to flood ensemble forecasts just last year \citep{Gregory}. 


\section{Methodology}

Given that the most expensive part of the forecast process is the black-box flood model, it is in our interests to only generate simulations of this model with optimised samples of spatio-temporal rainfall (first part). In this section of the report, we apply an optimization problem to generate trained samples of a certain catchment's spatio-temporal rainfall patterns that lead to predictions for rare flood events. As these are optimised samples for each catchment, one may only need to run the black-box model once for a `best guess' forecast or a handful times for the corresponding probabilistic forecast.

\subsection{Wetropolis}

Wetropolis is a flood demonstrator built under a Maths Foresees public outreach and engagement project \citep{Bokhove} and is an ideal toy problem for this study. We will consider the one-dimensional Wetropolis model in this report. The outline of the catchment, $x \in [0, L]$, is sketched in Figure \ref{figure:wetropolis}. It features two sources from separate coordinates along the catchment: an input from the reservoir and another input from the overflow of ground water. The coordinates of such locations are denoted $x_{1}$ and $x_{2}$. For simplicity in this study, the type of these sources will both be modelled as rainfall, and more importantly give us the ability to model spatially variant rainfall (albeit with only two degrees of freedom).

The model contains a city (between the coordinates $x_{\text{c}}^{-}$ and $x_{\text{c}}^{+}$) at the end of the catchment, and this is the area of interest for our flood forecasts. This model can be easily numerically discretized in a programming language and can run around ten seconds of real-time in approximately a single second on a laptop. Given that ten seconds in the model represents a day in `Wetropolis time', we can quickly and efficiently model flood events to test the methodology proposed in this study.

\begin{figure}[h!]
\centering
\includegraphics[width=12cm]{wetropolis1dschematic}
\caption{Schematic of the Wetropolis flood demonstrator in one dimension \citep{Bokhove}. Flow goes from left to right, with an outflow boundary at the right end of the catchment. The rainfall sources are located at the coordinates $x_{1}$ and $x_{2}$. The city is given by the red zone within the interval $[x_{c}^{-},x_{c}^{+}]$.}
\label{figure:wetropolis}
\end{figure}

\subsection{The damage metric}

In order to evaluate the impacts of a rare flood event and thus be able to quantify it, a damage metric, $D$, is defined. This damage metric will be a function of the flood water depth, over the area of interest: the city. In practice, rather than being based on the water depth, one would desire the damage metric to be primarily a function of the financial cost resulting from damage to the areas of interest in a catchment. However there is good reason to believe the two are positively correlated; a case study into certain catchments found a close coupling between the water depth from flooding and the financial cost avoided from installing barriers preventing floods \citep{Dale}.

We define the damage metric as the following integral quantity over the length of the city and time:
$$
D = \int_{t_{0}}^{t_{1}}\int_{x_{\text{c}}^{-}}^{x_{\text{city}}^{+}} \max \left(h(x)-h_{\text{c}}, 0\right)u(x,t) dxdt.
$$
Here $u(x,t)$ is the velocity of the flow and $[t_{0}, t_{1}]$ is the time interval of each flood event.

\subsection{Least squares minimization problem}


We define the piecewise constant functions of time $f_{1}$ and $f_{2}$ representing the temporal variation in rainfall at each of the two source coordinates respectively and thus the spatio-temporal variation in rainfall over the whole catchment. Thus there are six coefficients $\alpha_{i,j}$, $i=\{1,2\}$, $j=\{1,2,3\}$, in total making up both functions, $f_{i}(t)=\sum^{3}_{j=1}v_{i,j}(t)\alpha_{i,j}$. Then finally denote the function
$$
f(x,t)=f_{1}(t)\delta_{x_{1}-x} + f_{2}(t)\delta_{x_{2}-x}.
$$
The three degrees of freedom in each function represents the `start', `middle' and `end' of a given flood. Therefore
\begin{equation*}
v_{i,j}(t) =
\begin{cases}
1, & \frac{(j-1)(t_{1}-t_{0})}{3} \leq t - t_{0} \leq \frac{j(t_{1}-t_{0})}{3}\right],\\
0, & \text{otherwise}.
\end{cases}
\end{equation*}

The magnitude of rainfall at the two locations is denoted by $r(x,t)=r_{1}(t)\delta_{x-x_{1}}+r_{2}(t)\delta_{x-x_{2}}$; in the numerical simulation of Wetropolis, increments (in time) of $r_{1}$ and $r_{2}$ are drawn from a scalar distribution. In practice, they would typically come from scalar seasonal distributions \citep{EA}. For simplicity, we attribute each flood event, over $[t_{0},t_{1}]$, to a period of rainfall (a storm) from a fixed time period ago in the past, $r(x,t-\Delta t)$, $t \in [t_{0},t_{1}]$. In this study, this is based on training data from the Wetropolis model. It must be noted, however, that this is a crude assumption and doesn't account for multiple rain storms in quick succession causing long lasting floods.

We now explain how to train $f$ around data of flood events and use it to predict future flood events whilst capturing spatio-temporal patterns in rainfall over a catchment. Starting with \textit{ansatz}; our aim is to minimise $G$, defined below,

$$
G = \min_{f}\left\{\norm*{D-I(f)}^{2}\right\},
$$
over all possible $f$'s, where
$$
I(f) = \int_{t_{0}}^{t_{1}}\int_{0}^{L} f(x,t)r(x,t-\Delta t)dx dt.
$$
Given that we have a `training set` of $N$ flood events (continuous simulation of Wetropolis) we can frame the above minimization problem as a least squares minimization problem. Each recorded flood event, $i=1,...,N$, has the associated rainfall magnitude (at the time of the storm) $r_{i}(x,t-\Delta_{t})$, and define
$$
I_{i}(f) = \int_{t_{0}}^{t_{1}}\int_{0}^{L} f(x,t)r_{i}(x,t-\Delta t)dx dt.
$$
We also know the damage of each recorded flood event, $D_{i}$. Thus our minimization problem becomes
$$
\min_{f} \sum^{N}_{i=1}\left(D_{i}-I_{i}(f)\right)^2.
$$
One can solve a least squares minimization problem over $N$ flood events, to fit the coefficients $\alpha_{i, j}$, $i=\{1,2\}$, $j=\{1,2,3\}$, making up $f$.

\section{Results}

In this section we will optimise $f$ around a training set of flood events (continuous simulation of Wetropolis). This will then allow us to test the effectiveness of the method at constructing `best guess' forecasts of the damage of future flood events based on a testing set of rainfall storms. The effectiveness of the optimisation will be based on the improvement it offers over using an uniform $f$ over both rainfall locations and temporally constant throughout each flood.

Both spatially correlated and uncorrelated scenarios of training and testing rainfall and flood data will be used to confirm if the first two test cases highlighted in subsection \ref{sec:testcases} (uniform and spatially correlated) hold. As the rainfall data will be simulated continuously over time, the third case (spatio-temporal rainfall) is also tested. The correlated rainfall data will be produced by using an R package, \textit{RGLIMCLIM}, with two rainfall sites \citep{Chandler}.

The training sets of flood events are simulated over 5000 `Wetropolis' days, and the test set of flood events are simulated over 500 `Wetropolis' days.

\subsection{Spatially uncorrelated rainfall patterns}

In the uncorrelated rainfall case, the fitted function $f$ appears from Figure \ref{figure:uncorrelatedresults} to predict the damage from a particular storm event much more accurately than using an uniform $f$, especially in smaller flood events. In the rare, major flood events, the damage isn't predicted as well but predictions are still more accurate than in the uniform case.

\begin{figure}[h!]
\centering
\includegraphics[width=14cm]{uncorrelatedresults}
\caption{Predicted values of $I(f)$ with uniform $f$ (left panel) and optimised $f$ (right panel) against the damage metric, $D$, for the testing set of flood events with spatially uncorrelated rainfall patterns.}
\label{figure:uncorrelatedresults}
\end{figure}

\subsection{Spatially correlated rainfall patterns}

The test function $f$ becomes more important in the spatially correlated rainfall case, and it has to be able to capture the spatial dependence of the two rainfall locations to be able to accurately predict the damage of the test flood events. One can see from Figure \ref{figure:correlatedresults} that using the proposed method, fitting $f$ around the training data, does lead to being able to predict the damage of flood events better than that of using an uniform $f$. In fact, whilst there is little change of the predictive accuracy in smaller flood events, the root-mean-square-error (RMSE) from that of a perfect prediction is significantly improved in the case of using the optimised $f$ because of the larger flood events. Two main outliers effect the predictions when using an uniform $f$; these are much more constrained when using the optimised $f$.

\begin{figure}[h!]
\centering
\includegraphics[width=14cm]{correlatedresults}
\caption{Same as in Figure \ref{figure:uncorrelatedresults}, but for the testing set of flood events with spatially correlated rainfall patterns.}
\label{figure:correlatedresults}
\end{figure}

\newpage

\section{Conclusion and discussion}

In this report, we have proposed a method of capturing spatio-temporal patterns in rainfall for a given catchment and using this in forecasting flood events based on these storms. This `optimisation' over the spatio-temporally varying space is done before simulation of a black-box flood forecasting model and so is much more efficient than traditional Monte Carlo sampling strategies. The method has been shown to work on a simple proof of concept using an idealised two-rainfall-site model, Wetropolis, by leading to increased accuracy of predictions to flood events in cases where rainfall is both spatially correlated and uncorrelated.

We have left out the uncertainty quantification aspect of the least squares minimization problem used in this report for simplicity. This uncertainty quantification, by means of confidence interval around the least squares minimization, for example, could be utilised in this method to capture probability bands on our forecast. This still only requires very few runs of our black-box flood model; once again showing the method is more efficient than traditional Monte Carlo sampling ideas.



\vspace{4mm}

\bibliographystyle{plain}
\bibliography{refs}

.




\end{document}
